FROM ubuntu:20.04

MAINTAINER Kozhemiakin Kirill 'kozhemiakink@gmail.com'

USER root
WORKDIR /home/simple-reviews

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install -y python3-pip python3-dev build-essential libpq-dev
COPY requirements.txt .
RUN pip3 install setuptools
RUN pip3 install -r requirements.txt
RUN rm requirements.txt
COPY main.py .
COPY migrate.sh .
RUN chmod +x migrate.sh
COPY ./revapp /home/simple-reviews/revapp
