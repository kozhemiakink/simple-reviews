Description
----------
Simple reviews - a simple web application to demonstrate the capabilities of flask. This project will introduce you to the basic structure of a backend project.

Technology stack - _Flask_, _sqlalchemy_+_PostrgeSQL_, _wtforms_, _jinja2_, _webargs_, _docker-compose_.

Availible API routes

``POST   /api/v1/reviews``             - create new review;

``GET    /api/v1/reviews/{review_id}`` - get review by id;

``DELETE /api/v1/reviews/{review_id}`` - delete review by id;

``POST /api/v1/reviews-many``          - create review array;

``GET /api/v1/reviews-many``           - get review array;

Availible routes

``POST   /new-review``     - new review form;

``GET    /reviews/{page}`` - show reviews by pages;

Installation
----------

Clone project.

    git clone https://gitlab.com/kozhemiakink/simple-reviews.git
    cd simple-file-storage

Before running project, set settings through enviroment variables

    export SECRET_KEY='app secret key'
    export POSTGRES_DB='revapp_db'
    export POSTGRES_HOST='postgres'
    export POSTGRES_USER='revapp_user'
    export POSTGRES_PASSWORD='user_pass'

Than run project
- with docker-compose
    ```
    docker-compose up -d
    ```
- with gunicorn :warning: Before running this application you have to set up the PostgreSQL
    ```
    apt update && \
    apt install -y --no-install-recommends \
    python3 python3-dev python3-pip python3-venv python3-wheel build-essential libpq-dev
    python3 -m venv env && . env/bin/activate
    pip3 install setuptools wheel
    pip3 install -r requirements.txt
    chmod +x migrate.sh && ./migrate.sh
    gunicorn -b 0.0.0.0:14321 "revapp.app:create_app('revapp.config.ProductionConfig')" --daemon
    ```

Curl testing
-------------

Create new review through API

    curl 'http://localhost:14321/api/v1/reviews' -X POST -H "Content-Type: application/json" \
    --data-raw '{"name": "Slava", "text": "I hate this site", "score": "1"}'

Get review details (use your review id)

    curl 'http://localhost:14321/api/v1/reviews/3'

Delete review (use your review id)

    curl -X DELETE 'http://localhost:14321/api/v1/reviews/3'

After deleting you should get 404 error

    curl -I 'http://localhost:14321/api/v1/reviews/3'

Create many reviews

    curl 'http://localhost:14321/api/v1/reviews-many' -X POST -H "Content-Type: application/json" \
    --data-raw '[
        {"name": "Andrew", "text": "my first review", "score": "4"},
        {"name": "Andrew", "text": "my second review", "score": "4"}]'

Get all reviews

    curl 'http://localhost:14321/api/v1/reviews-many'
