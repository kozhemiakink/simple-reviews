import os


class BaseConfig:
    ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

    SECRET_KEY = os.environ.get('SECRET_KEY', 'A SECRET KEY')
    REVIEW_PAGE_SIZE = int(os.environ.get('REVIEW_PAGE_SIZE', 5))

    POSTGRES_HOST = os.environ.get('POSTGRES_HOST', 'localhost')
    POSTGRES_DB = os.environ.get('POSTGRES_DB', 'reviews_db')
    POSTGRES_USER = os.environ.get('POSTGRES_USER', 'root')
    POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD', '')
    SQLALCHEMY_DATABASE_URI = (
        f'postgresql://{POSTGRES_USER}:'
        f'{POSTGRES_PASSWORD}@{POSTGRES_HOST}/{POSTGRES_DB}'
    )


class DevelopementConfig(BaseConfig):
    DEBUG = True
    SQLALCHEMY_ECHO = True


class ProductionConfig(BaseConfig):
    DEBUG = False

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
