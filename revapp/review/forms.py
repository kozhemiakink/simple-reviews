from flask_wtf import FlaskForm
import wtforms.fields as fields
import wtforms.validators as validators


class ReviewForm(FlaskForm):
    name = fields.StringField(
        'Name',
        validators=[
            validators.Length(min=1, max=32),
            validators.Regexp(r'^(?![_. ])(?!.*[_. ]{2})[А-яЁёa-zA-Z0-9._ ]+(?<![_. ])$')
        ]
    )
    text = fields.TextAreaField('Review', validators=[validators.Length(min=1, max=3000)])
    score = fields.RadioField(
        'Score',
        validators=[validators.DataRequired()],
        choices=[(i, str(i)) for i in range(1, 6)]
    )
    submit = fields.SubmitField('Submit')

class ReviewFiltresForm(FlaskForm):
    order = fields.SelectField(
        'Order',
        validators=[validators.DataRequired()],
        choices=[('asc', 'Ascending'), ('desc', 'Descending')]
    )
    sort_by = fields.SelectField(
        'Sort by',
        validators=[validators.DataRequired()],
        choices=[
            ('id', 'Serial Number'),
            ('name', 'Reviewer Name'),
            ('text', 'Review Text'),
            ('score', 'Review Score')
        ]
    )
    submit = fields.SubmitField('Search')
