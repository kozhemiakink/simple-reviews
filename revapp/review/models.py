from revapp.mixins import CRUDMixin
from revapp.extensions import db


class Review(db.Model, CRUDMixin):
    __tablename__ = 'reviews'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)
    text = db.Column(db.Text, nullable=False)
    score = db.Column(db.SmallInteger, nullable=False)
