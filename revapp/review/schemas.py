from markupsafe import escape
from marshmallow import Schema, fields, validate, post_load


class ReviewSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(
        required=True,
        validate=[
            validate.Length(min=1, max=32),
            validate.Regexp(r'^(?![_. ])(?!.*[_. ]{2})[А-яЁёa-zA-Z0-9._ ]+(?<![_. ])$')
        ]
    )
    text = fields.Str(required=True, validate=validate.Length(min=1, max=3000))
    score = fields.Int(required=True, validate=validate.OneOf(tuple(range(1, 6))))

    @post_load
    def cleaning_text(self, in_data, **kwargs):
        in_data['text'] = escape(in_data['text'])
        return in_data


class ReviewFiltresSchema(Schema):
    order = fields.Str(
        required=False,
        validate=validate.OneOf(('asc', 'desc')),
        missing='asc'
    )
    sort_by = fields.Str(
        required=False,
        validate=validate.OneOf(('id', 'name', 'text', 'score')),
        missing='id'
    )
