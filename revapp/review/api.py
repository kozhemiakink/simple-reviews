from sqlalchemy import desc, asc
from flask import jsonify, current_app, abort
from webargs.flaskparser import use_args

from revapp.extensions import db
from revapp.review.models import Review
from revapp.review.schemas import ReviewSchema, ReviewFiltresSchema



@use_args(ReviewSchema(), location='json')
def review_create(review):
    review = Review.create(**review)
    return jsonify(ReviewSchema().dump(review)), 201


def review_get(review_id):
    review = Review.get_by_id(review_id)
    if review is None:
        abort(404)

    return jsonify(ReviewSchema().dump(review))


def review_delete(review_id):
    review = Review.get_by_id(review_id)
    if review is not None:
        review.delete()

    return '', 204


@use_args(ReviewSchema(many=True), location='json')
def review_create_many(reviews):
    review_created = []
    for review in reviews:
        review_created.append(Review.create(**review))
        db.session.add(review_created[-1])

    db.session.flush()
    db.session.commit()

    return jsonify(ReviewSchema(many=True).dump(review_created)), 201


@use_args(ReviewFiltresSchema(), location='query')
def review_get_many(filtres):
    order = filtres['order']
    sort_by = filtres['sort_by']

    if sort_by == 'id':
        column = Review.id
    elif sort_by == 'name':
        column = Review.name
    elif sort_by == 'text':
        column = Review.text
    else:
        column = Review.score

    if order == 'desc':
        sort_by = desc(column)
    else:
        sort_by = asc(column)

    reviews = Review.query.order_by(sort_by).all()

    return jsonify(ReviewSchema(many=True).dump(reviews))
