from markupsafe import escape
from sqlalchemy import desc, asc
from flask import (Blueprint, current_app,
    render_template, redirect, url_for)
from webargs.flaskparser import use_kwargs

import revapp.review.api as api
from revapp.extensions import db
from revapp.review.models import Review
from revapp.review.forms import ReviewForm
from revapp.review.schemas import ReviewFiltresSchema


blueprint = Blueprint('review', __name__)

blueprint.add_url_rule('/api/v1/reviews', view_func=api.review_create, methods=('POST',))
blueprint.add_url_rule('/api/v1/reviews/<int:review_id>', view_func=api.review_get, methods=('GET',))
blueprint.add_url_rule('/api/v1/reviews/<int:review_id>', view_func=api.review_delete, methods=('DELETE',))
blueprint.add_url_rule('/api/v1/reviews-many', view_func=api.review_create_many, methods=('POST',))
blueprint.add_url_rule('/api/v1/reviews-many', view_func=api.review_get_many, methods=('GET',))


@blueprint.route('/', methods=('GET',))
def root():
    return redirect(url_for('review.reviews', page=1))


@blueprint.route('/new-review', methods=('GET', 'POST'))
def new_review():
    form = ReviewForm()

    if form.validate_on_submit():
        name = form.name.data
        text = escape(form.text.data)
        score = form.score.data

        Review.create(
            name=name,
            text=text,
            score=score
        )

        return redirect(url_for('review.reviews', page=1))

    return render_template('review/new_review.html', form=form)


@blueprint.route('/reviews/<int:page>', methods=('GET',))
@use_kwargs(ReviewFiltresSchema(), location='query')
def reviews(page, order, sort_by):
    if sort_by == 'id':
        column = Review.id
    elif sort_by == 'name':
        column = Review.name
    elif sort_by == 'text':
        column = Review.text
    else:
        column = Review.score

    if order == 'asc':
        column = asc(column)
    else:
        column = desc(column)

    reviews = (
        Review
        .query
        .order_by(column)
        .paginate(
            page,
            current_app.config['REVIEW_PAGE_SIZE'],
            error_out=False
        )
    )

    return render_template(
        'review/reviews.html',
        reviews=reviews,
        order=order,
        sort_by=sort_by
    )
