from flask import Flask

from revapp.extensions import db, migrate
from revapp import review


def create_app(config_path):
    app = Flask(__name__)
    app.config.from_object(config_path)

    register_extensions(app)
    register_shellcontext(app)
    register_blueprints(app)
    register_commands(app)
    return app


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)


def register_blueprints(app):
    app.register_blueprint(review.views.blueprint)


def register_shellcontext(app):
    def shell_context():
        return {
            'db': db,
            'Review': review.models.Review,
            'ReviewForm': review.forms.ReviewForm,
            'ReviewFiltresForm': review.forms.ReviewFiltresForm,
            'ReviewSchema': review.schemas.ReviewSchema,
            'ReviewFiltresSchema': review.schemas.ReviewFiltresSchema,
        }

    app.shell_context_processor(shell_context)


def register_commands(app):
    pass
