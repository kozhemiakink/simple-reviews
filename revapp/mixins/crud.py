from revapp.extensions import db


class CRUDMixin:
    __table_args__ = {'extend_existing': True}

    @classmethod
    def get_by_id(cls, id):
        if (isinstance(cls.__table__.columns['id'].type, db.String)
            and isinstance(id, str)
        ):
            return cls.query.get(id)
        elif any(
            (isinstance(id, str) and id.isdigit(),
             isinstance(id, int)),
        ):
            return cls.query.get(int(id))

        return None

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        db.session.delete(self)
        return commit and db.session.commit()
